import "./App.css";

import { useState, useEffect } from "react";

import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import { connect } from "react-redux";

import CreateAlbumForm from "./CreateAlbumForm";
import { fetchAlbums } from "../actions";
import AlbumsListing from "./AlbumsListing";

const App = ({ fetchAlbums }) => {
	//const [albums, setAlbums] = useState([]);
	const [fetchAlbum, setFetchAlbum] = useState(false);

	useEffect(() => {
		fetchAlbums();
		console.log("App mounted");
		// axios.get("http://localhost:8080/statusapiSom/all").then(
		// 	(res) => {
		// 		// setAlbums(res.data);
		// 		fetchAlbumSuccess(res.data);
		// 	},
		// 	() => {
		// 		//
		// 	}
		// );
	}, [fetchAlbum, fetchAlbums]);

	return (
		<Router>
			<div className="app">
				<nav>
					<ul>
						<li>
							<Link to="/">Home</Link>
						</li>
						<li>
							<Link to="/add">Add Album</Link>
						</li>
					</ul>
				</nav>

				<Switch>
					<Route exact path="/">
						<AlbumsListing />
					</Route>
					<Route path="/add">
						<CreateAlbumForm
							fetchAlbum={fetchAlbum}
							setFetchAlbum={setFetchAlbum}
						/>
					</Route>
				</Switch>

				{/* <Banner /> */}
			</div>
		</Router>
	);
};

const mapStateToProps = (state) => {
	return {
		albums: state.albums,
	};
};

const actionCreators = {
	fetchAlbums,
};

export default connect(mapStateToProps, actionCreators)(App);
