import React from "react";

const AlbumArtist = (props) => {
	return <h2>{props.artist}</h2>;
};

export default AlbumArtist;
