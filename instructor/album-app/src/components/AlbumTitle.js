import React from "react";
import ReactDOM from "react-dom";

const AlbumTitle = (props) => {
	return <h2>{props.title}</h2>;
};

export default AlbumTitle;
