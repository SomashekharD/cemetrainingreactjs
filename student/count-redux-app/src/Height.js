import React from "react";
import { connect } from "react-redux";
const Height = (props) => (
	<div>
		<p>Height is {props.height}</p>
	</div>
);

const mapStateToProps = (state) => {
	console.log("state is ", state);
	return {
		count: state.count,
		height: state.height,
	};
};
export default connect(mapStateToProps)(Height);
