import React from "react";
import { connect } from "react-redux";
import { addCount, decrement } from "./actions";
import Height from "./Height";

const App = (props) => (
	//console.log("App props is ", props);
	<React.Fragment>
		<div>App component</div>
		<p>
			Count is {props.count}{" "}
			<button onClick={() => props.addCount(props.count + 1)}>Add Count</button>
		</p>
		<p>Weight is {props.weight}</p>
		<Height></Height>
	</React.Fragment>
);

// mapStateToProps receives the redux store/state
const mapStateToProps = (state) => {
	console.log("state is ", state);
	return {
		count: state.count,
		weight: state.weight,
	};
};
const actionCreators = {
	addCount,
	decrement,
};

export default connect(mapStateToProps, actionCreators)(App);
