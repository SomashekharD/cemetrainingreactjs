import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";

import App from "./App";

// state is always undefined at the start
const countReducer = (state = 0, action) => {
	if (action.type === "ADD_COUNT") {
		return action.payload;
	}
	console.log("receved ");

	return state;
};

const weightReducer = (state = 100, action) => {
	return state;
};

const heightReducer = (state = 185, action) => {
	return state;
};

const reducers = combineReducers({
	count: countReducer,
	weight: weightReducer,
	height: heightReducer,
});

const store = createStore(reducers);
console.log("store is ", store.getState());
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("root")
);
