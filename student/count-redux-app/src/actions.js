export const addCount = (count) => {
	return {
		type: "ADD_COUNT",
		payload: count,
	};
};

export const decrement = (count) => {
	return {
		type: "DECREMENT_COUNT",
	};
};
