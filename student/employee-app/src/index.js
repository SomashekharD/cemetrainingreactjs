import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "bootstrap/dist/css/bootstrap.css";
import { createStore, combineReducers, applyMiddleware } from "redux";
import employeesReducer from "./reducers/employeesReducer";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";

const reducers = combineReducers({
	employees: employeesReducer,
});

const store = createStore(reducers, applyMiddleware(thunkMiddleware));
console.log("store is ", store.getState());
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("root")
);
