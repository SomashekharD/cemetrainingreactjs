const initialState = { employees: [] };

export default (state = initialState, action) => {
	console.log(`received ${action.type} dispatch in albumsReducer`);
	switch (action.type) {
		case "FETCH_EMPLOYEES_SUCCESS":
			return { ...state, employees: action.payload, loading: false };
		case "FETCH_EMPLOYEES_FAILURE":
			return { ...state, employees: [], loading: false, error: action.payload };

		default:
			return state;
	}
};

//state is looking like this
// {
//     albums: {
//         entities: [],
//         loading: false,
//         error: "failed to fetch data"
//     },
//     errors : ["failed to fetch data", "failed to add albums"],
//     user: {
//         name: "john"
//     },
//     cart: []
// }
