import React, { useState } from "react";

import EmployeeName from "./EmployeeName";
import EmployeeInfo from "./EmployeeInfo";
import ShowHideButton from "./ShowHideButton";

const Album = (props) => {
	const [visible, setVisibity] = useState(true);

	return (
		<div className="col-md-4">
			<div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
				<div className="card-body">
					<EmployeeName name={props.name} />
					<EmployeeInfo info={props.info} visible={visible} />
					<ShowHideButton toggle={setVisibity} visible={visible} />
				</div>
			</div>
		</div>
	);
};

export default Album;
