import React from "react";

const EmployeeInfo = (props) => {
	return (
		props.visible && (
			<ul className="album-tracks">
				<li>Age : {props.info.age}</li>
				<li>Email :{props.info.email}</li>
				<li>Salary :{props.info.salary}</li>
			</ul>
		)
	);
};

export default EmployeeInfo;
