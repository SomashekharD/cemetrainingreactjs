import React from "react";

const EmployeeName = (props) => {
	return <h2>{props.name}</h2>;
};

export default EmployeeName;
