import axios from "axios";

// for our Action Creators
export const fetchEmployeesBegin = () => {
	return {
		type: "FETCH_Employees_BEGIN",
	};
};

export const fetchEmployeesSuccess = (employees) => {
	return {
		type: "FETCH_EMPLOYEES_SUCCESS",
		payload: employees,
	};
};

export const fetchEmployeesFailure = (err) => {
	return {
		type: "FETCH_EMPLOYEES_FAILURE",
		payload: { message: "Failed to fetch albums.. please try again later" },
	};
};

// to be call by the components
export const fetchEmployees = () => {
	// returns the thunk function
	return (dispatch, getState) => {
		dispatch(fetchEmployeesBegin());
		console.log("state after fetchAlbumsBegin", getState());
		axios.get("http://localhost:8080/statusapiSom/all").then(
			(res) => {
				//console.log(res);
				//setAlbums(res.data); // TODO dispatch FETCH_ALBUMS_SUCCESS
				setTimeout(() => {
					dispatch(fetchEmployeesSuccess(res.data));
					console.log("state after fetchAlbumsSuccess", getState());
				}, 3000);
			},
			(err) => {
				// dispatch FETCH_ALBUMS_FAILURE
				dispatch(fetchEmployeesFailure(err));
				console.log("state after fetchAlbumsFailure", getState());
			}
		);
	};
};
