import "./App.css";
import { useEffect } from "react";
import Employee from "./Employee";
// import axios from "axios";
import { connect, useDispatch, useSelector } from "react-redux";
import { fetchEmployees } from "./actions/index";

const App = (props) => {
	const dispatch = useDispatch();
	// const [employees, setEmployees] = useState([]);
	const employees = useSelector((state) => state.employees.employees);
	useEffect(() => {
		console.log("dispatching api call");
		// dispatch(fetchEmployees());
	}, [employees]);
	return (
		<div className="app">
			<div className="container">
				<div className="row">
					{employees.map((employee) => (
						<Employee key={employee.id} name={employee.name} info={employee} />
					))}
				</div>
			</div>
			<button onClick={() => dispatch(fetchEmployees())}>Load Employees</button>
		</div>
	);
};

const mapStateToProps = (state) => {
	console.log("App state is ", state);
	return {
		employees: state.employees.employees,
	};
};

const actionCreators = {
	fetchEmployees,
};

export default connect(mapStateToProps, actionCreators)(App);
