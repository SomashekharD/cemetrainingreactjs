import React from "react";

class Greet extends React.Component {
	// constructor(props) {
	// 	super(props);
	// }
	render() {
		return (
			<>
				<h1>Hello {this.props.name + "  "} </h1>
				<h2>{this.props.height} </h2>
			</>
		);
	}
}

export default Greet;

// const Greet = (props) => {
// 	return <h1>Hello {props.name}</h1>;
// };
