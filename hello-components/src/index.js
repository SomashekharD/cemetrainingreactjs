import React, { useState } from "react";
import ReactDOM, { render } from "react-dom";
import Greet from "./Greet";
// import Person from "./Person";

const PersonFunc = () => {
	const [height, setHeight] = useState(185);
	// const [weight, setWeight] = useState();

	return (
		<div>
			<Greet name="Som" height={height} />
			<button onClick={() => setHeight(height + 1)}>Grow</button>
		</div>
	);
};

const main = (
	<div>
		<PersonFunc />
	</div>
);
ReactDOM.render(main, document.querySelector("#root"));
