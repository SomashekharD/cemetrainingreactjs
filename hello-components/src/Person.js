import React from "react";

class Person extends React.Component {
	constructor(props) {
		super(props);
		this.state = { height: 185, weight: "200" };
	}

	grow = () => {
		//this.setState({ height: this.state.height + 1 });
		this.setState((state) => {
			return {
				height: state.height + 1,
			};
		});
		this.setState((state) => {
			return {
				height: state.height + 1,
			};
		});
	};

	componentDidUpdate() {
		console.log("rendered");
	}
	render() {
		return (
			<div>
				<h1>{this.state.height}</h1>
				<button onClick={this.grow}>Grow</button>
			</div>
		);
	}
}
export default Person;
